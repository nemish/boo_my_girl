﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shelter : MonoBehaviour
{
    public string canHideText = "Спрятаться (F)";
    public string getOutText = "Вылезти (F)";

    [Tooltip("Проникновения звука из укрытия наружу")]
    public bool SoundCanGoOut;

    [Tooltip("Проникновение “запаха” из укрытия наружу. Данный параметр означает: если перс прячется в укрытие в момент агра монстра, возможность монстра найти перса.")]
    public bool SmellCanGoOut;

    [Range(0, 100)]
    [Tooltip("Видимость из укрытия: коэфф видимости экрана.")]
    public float Visibility;

    public HidingType HidingType;

    public GameObject BlackImagePrefab;

    private GameObject _hint;
    private TextMesh _hintText;
    private GirlMovement _girlMovement;
    private Shelter _shelterApi;


    private void Start()
    {
        _girlMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<GirlMovement>();
        _shelterApi = transform.GetComponent<Shelter>();
        _hint = transform.Find("ShelterHint").gameObject;
        _hintText = _hint.transform.GetComponent<TextMesh>();
        ResetHint();
        _hint.SetActive(false);
    }

    private void ResetHint()
    {
        _hintText.text = canHideText;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && !_hint.activeSelf)
        {
            _hint.SetActive(true);
            _girlMovement.SetAvailableShelter(_shelterApi);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            _girlMovement.SetAvailableShelter(null);
            StartCoroutine("HideHint");
        }
    }

    private IEnumerator HideHint()
    {
        yield return new WaitForSeconds(0.5f);
        _hint.SetActive(false);
    }

    public void SetOccupied()
    {
        _hintText.text = getOutText;

    }

    public void SetVacant()
    {
        ResetHint();
    }
}