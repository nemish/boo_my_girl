﻿using UnityEngine;
using System.Collections;

public class MainCanvas : MonoBehaviour {

    public GameObject gunImage;
    public GameObject gunImageEmpty;

	void Start () {
        gunImage = transform.Find("GunImage").gameObject;
        gunImageEmpty = transform.Find("GunImageEmpty").gameObject;
        gunImage.SetActive(false);
	}

    public void SetHasGun()
    {
        gunImageEmpty.SetActive(false);
        gunImage.SetActive(true);
    }
}
