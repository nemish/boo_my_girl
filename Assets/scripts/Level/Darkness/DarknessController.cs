﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.scripts.Level
{
    public class DarknessController : MonoBehaviour
    {
        private SpriteRenderer _renderer;
        public float ShowSpeed;
        public float UnhideInterval;

        private void Start()
        {
            _renderer = GetComponent<SpriteRenderer>();
        }

        public void Show()
        {
            StopCoroutine("RemoveDarkness");
            StopCoroutine("AddDarkness");
            StartCoroutine("RemoveDarkness");
        }

        public void Hide()
        {
            StopCoroutine("RemoveDarkness");
            StopCoroutine("AddDarkness");
            StartCoroutine("AddDarkness");
        }

        private IEnumerator RemoveDarkness()
        {
            while (_renderer.color.a > 0)
            {
                Color black = Color.black;
                black.a = _renderer.color.a - ShowSpeed;
                _renderer.color = black;

                Debug.Log(black.a);

                yield return new WaitForSeconds(UnhideInterval);
            }
        }

        private IEnumerator AddDarkness()
        {
            while (_renderer.color.a < 1)
            {
                Color black = Color.black;
                black.a = _renderer.color.a + ShowSpeed;
                _renderer.color = black;

                Debug.Log(black.a);

                yield return new WaitForSeconds(UnhideInterval);
            }
        }
    }
}