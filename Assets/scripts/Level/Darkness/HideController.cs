﻿using System.Collections;
using UnityEngine;

public class HideController : MonoBehaviour
{
    public GameObject DarknessGameObject;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && DarknessGameObject != null)
        {
            Debug.Log("Hiding.");
            DarknessGameObject.SendMessage("Hide");
        }
    }
}