﻿using System.Collections;
using UnityEngine;

public class ShowController : MonoBehaviour
{
    public GameObject DarknessGameObject;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && DarknessGameObject != null)
        {
            Debug.Log("Showing.");
            DarknessGameObject.SendMessage("Show");
        }
    }
}