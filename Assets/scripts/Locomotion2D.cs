﻿using UnityEngine;
using System.Collections;

public class Locomotion2D : Locomotion {
    public bool facingRight = true;

    protected override void UpdateMoveDirection()
    {
        base.UpdateMoveDirection();
        UpdateFacing();
    }

    protected virtual void UpdateFacing()
    {
        facingRight = direction.x > 0;
    }
}
