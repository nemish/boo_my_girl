﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public float health = 100f;

	void Update () {
        if (health < 0f)
        {
            Dead();
        }
	}

    void Dead()
    {
        Destroy(gameObject);
    }

    public void ReceiveDamage(float damage)
    {
        health -= damage;
    }
}
