﻿using UnityEngine;
using System.Collections;

public class EnemyAttackSense : MonoBehaviour {

    private GirlMovement playerAPI;
    private EnemyMovement enemyAPI;

    void Start()
    {
        if (!playerAPI)
        {
            playerAPI = GameObject.FindGameObjectWithTag("Player").GetComponent<GirlMovement>();
        }

        if (!enemyAPI)
        {
            enemyAPI = transform.parent.GetComponent<EnemyMovement>();
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Player")
        {
            if (IsPlayerInSight(col.transform))
            {
                enemyAPI.target = col.transform;
            }
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            enemyAPI.target = null;
        }
    }

    private bool IsPlayerInSight(Transform player)
    {
        return enemyAPI.facingRight && (player.position.x - transform.position.x) > 0
            || !enemyAPI.facingRight && (player.position.x - transform.position.x) < 0;
    }
}
