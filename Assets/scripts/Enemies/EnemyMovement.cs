﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyMovement : Locomotion2D {

    public List<float> patrolPointsDeltas = new List<float>();
    protected List<Vector3> patrolPoints = new List<Vector3>();

    protected EnemyAttackSense attackSense;

    protected Vector3 nextPatrolPoint = Vector3.zero;

    public override void Initialize()
    {
        base.Initialize();
        attackSense = transform.Find("AttackSense").GetComponent<EnemyAttackSense>();
        ToPatrol();
        if (patrolPointsDeltas.Count == 0)
        {
            patrolPointsDeltas.Add(4f);
            patrolPointsDeltas.Add(-4f);
        }

        foreach (float ppDelta in patrolPointsDeltas)
        {
            patrolPoints.Add(GetNextPatrolPoint(ppDelta));
        }
        nextPatrolPoint = patrolPoints[0];
        maxMovementSpeed = 0.5f;
        maxAttackMoveSpeed = 4f;
    }

    protected override void ProcessState()
    {
        if (IsAttacking())
        {
            Attack();
        }
        else if (IsAttackPreparing())
        {
            AttackPreparing();
        }
        else if (IsPatroling())
        {
            Patrol();
        }
        else
        {
            Idle();
        }
    }

    protected override void UpdateMoveDirection()
    {
        if (target)
        {
            direction = target.position - transform.position;
            if (!IsAttacking())
            {
                ToAttackPreparing();
            }
        }
        else if (nextPatrolPoint != Vector3.zero)
        {
            direction = nextPatrolPoint - transform.position;
            ToPatrol();
        }
        else
        {
            direction = Vector3.zero;
            ToIdle();
        }
        direction = direction.normalized;
        base.UpdateMoveDirection();
    }

    protected void Patrol()
    {
        if (Vector3.Distance(nextPatrolPoint, transform.position) < 0.2f)
        {
            int nextPointIndex = patrolPoints.IndexOf(nextPatrolPoint) + 1;
            if (patrolPoints.Count == nextPointIndex)
            {
                nextPointIndex = 0;
            }
            nextPatrolPoint = patrolPoints[nextPointIndex];
        }
        currentSpeedLimit = maxMovementSpeed;
        CommonMovement();
    }

    protected bool IsPatroling()
    {
        return currentState == states.Patrol;
    }

    private Vector3 GetNextPatrolPoint(float offset)
    {
        return new Vector3(oPosition.x - offset, oPosition.y, oPosition.z);
    }

    protected override void AttackPreparing()
    {
        currentSpeedLimit = Mathf.Lerp(currentSpeedLimit, maxAttackMoveSpeed, Time.deltaTime);
        CommonMovement();

        if (Vector3.Distance(transform.position, target.position) < 1.5f)
        {
            ToAttacking();
        }
    }

	public override void ReactToNoise(Transform source){
		target = source;
	}

    protected virtual void ToPatrol()
    {
        currentState = states.Patrol;
    }

    protected override void Attack()
    {
        Application.LoadLevel(Application.loadedLevelName);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Player" && target == null)
        {
            target = collision.transform;
        }
    }
}
