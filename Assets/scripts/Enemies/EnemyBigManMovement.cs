﻿using UnityEngine;
using System.Collections;

public class EnemyBigManMovement : EnemyMovement {
    private Animator anim;

    public override void Initialize()
    {
        base.Initialize();
        anim = transform.Find("monster1_mesh").GetComponent<Animator>();
    }

    protected override void CommonMovement()
    {
        base.CommonMovement();
        anim.SetFloat("Speed", direction.magnitude);
    }
}
