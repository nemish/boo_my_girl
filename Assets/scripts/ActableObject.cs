﻿using UnityEngine;
using System.Collections;

public class ActableObject : MonoBehaviour {

    public virtual Transform GetActionPoint(Transform player) {
        return transform;
    }

    public virtual Vector3 GetFacingDirectionForAction(Transform point) {
        return Vector3.zero;
    }

    public virtual Vector3 GetDirectionToActionPoint(Transform player, Transform point) {
        return Vector3.zero;
    }
}
