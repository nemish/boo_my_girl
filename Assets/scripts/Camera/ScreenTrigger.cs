﻿using UnityEngine;
using System.Collections;

public class ScreenTrigger : MonoBehaviour {

    public GameObject oppositeScreenTrigger;
    public GameObject screenAdditionalTrigger;
    private float cameraOffset;
    private FixedCamera cameraAPI;

    void Start ()
    {
        cameraOffset = transform.position.x - transform.parent.position.x;
        cameraAPI = transform.parent.GetComponent<FixedCamera>();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            MoveCameraScreen(col);
        }
    }

    public void MoveCameraScreen(Collider col)
    {
        Locomotion playerAPI = col.transform.GetComponent<Locomotion>();
        playerAPI.ToIdle();
        playerAPI.enabled = false;
        oppositeScreenTrigger.SetActive(false);
        cameraAPI.MoveScreen(cameraOffset, screenAdditionalTrigger);
    }
}
