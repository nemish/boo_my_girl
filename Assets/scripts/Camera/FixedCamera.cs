﻿using UnityEngine;
using System.Collections;

public class FixedCamera : MonoBehaviour {

    public float cameraRotationRatio = 10f;
    public Transform leftScreenTrigger;
    public Transform rightScreenTrigger;
    private Transform player;
    private Vector3 offset;
    private Quaternion oRotation;
    private float triggerOffset;
    private Vector3 screenPosition;
    private GameObject currentAdditionalTrigger;

    void Start () {
        player = GameObject.FindGameObjectsWithTag("Player")[0].transform;
        oRotation = transform.rotation;
        screenPosition = transform.position;
    }

    void FixedUpdate () {
        Quaternion rotationAngle = Quaternion.Euler(0, (player.position.x - transform.position.x) / cameraRotationRatio, 0);
        transform.rotation = Quaternion.Lerp(transform.rotation, oRotation * rotationAngle, Time.deltaTime * 10f);
        transform.position = Vector3.Lerp(transform.position, screenPosition, Time.deltaTime * 1.5f);
        if (Vector3.Distance(transform.position, screenPosition) < 0.5f)
        {
            player.GetComponent<Locomotion>().enabled = true;
            if (currentAdditionalTrigger)
            {
                currentAdditionalTrigger.SetActive(true);
            }
        }
    }

    public void MoveScreen(float offset, GameObject trigger)
    {
        screenPosition = new Vector3(transform.position.x + offset * 2, transform.position.y, transform.position.z);
        currentAdditionalTrigger = trigger;
    }
}
