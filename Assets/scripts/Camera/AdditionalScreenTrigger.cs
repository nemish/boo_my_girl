﻿using UnityEngine;
using System.Collections;

public class AdditionalScreenTrigger : MonoBehaviour {

    public GameObject screenTrigger;

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            screenTrigger.GetComponent<ScreenTrigger>().MoveCameraScreen(col);
        }
    }
}
