﻿using UnityEngine;
using System.Collections;

public class ScreenTriggerExit : MonoBehaviour {

    public GameObject screenTrigger;
    public GameObject screenAdditionalTrigger;

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            StartCoroutine("ActivateTrigger");
        }
    }

    IEnumerator ActivateTrigger()
    {
        yield return new WaitForSeconds(0.1f);
        screenTrigger.SetActive(true);
        screenAdditionalTrigger.SetActive(false);
    }

}
