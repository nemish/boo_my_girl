﻿using UnityEngine;
using System.Collections;

public class PickupObjectTrigger : MonoBehaviour {

    private PickupObject _pickupObjectAPI;

    void Start()
    {
        _pickupObjectAPI = transform.parent.GetComponent<PickupObject>();
    }

    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Player")
        {
            _pickupObjectAPI.ShowHint();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine("HideHint");
        }
    }

    private IEnumerator HideHint()
    {
        yield return new WaitForSeconds(0.5f);
        _pickupObjectAPI.HideHint();
    }

}
