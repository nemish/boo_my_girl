﻿using UnityEngine;

public class DirectInput : UserInput
{
    private LayerMask actableObjectsLM;
    private Transform selectedObject;

    protected override void InitializeCommon()
    {
        base.InitializeCommon();
        actableObjectsLM = LayerMask.GetMask("Actable");
    }

    public void ProcessActionsInput()
    {
        if (Input.GetButtonUp("HandleShelter"))
        {
            playerAPI.HandleShelter();
        }

        if (Input.GetButton("Run"))
        {
            playerAPI.SetRunMovement();
        }
        else if (Input.GetButton("Sneak"))
        {
            playerAPI.SetSneakMovement();
        }
        else
        {
            playerAPI.SetCommonMovement();
        }
    }

    public void ProcessMouseInput()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, actableObjectsLM))
        {
            selectedObject = hit.transform;
        }

        if (Input.GetMouseButton(1))
        {
            playerAPI.SetActableObject(selectedObject);
        }
        else
        {
            playerAPI.SetActableObject(null);
        }
    }
}
