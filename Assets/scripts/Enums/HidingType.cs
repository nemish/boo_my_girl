﻿[System.Serializable]
public enum HidingType
{
    Vertical,
    Horizontal
}