﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DraggableObject : ActableObject {

    public Transform leftPullPoint;
    public Transform rightPullPoint;
    public Transform leftEdge;
    public Transform rightEdge;

    private Dictionary<Transform, Transform> navPointsDict = new Dictionary<Transform, Transform>();

    void Start()
    {
        navPointsDict.Add(leftPullPoint, leftEdge);
        navPointsDict.Add(rightPullPoint, rightEdge);
    }

    public override Transform GetActionPoint(Transform player)
    {
        return GetNearestDragPoint(player);
    }

    private Transform GetNearestDragPoint(Transform player)
    {
        if (Vector3.Distance(player.position, leftPullPoint.position) < Vector3.Distance(player.position, rightPullPoint.position))
        {
            return leftPullPoint;
        }
        else
        {
            return rightPullPoint;
        }
    }

    public override Vector3 GetFacingDirectionForAction(Transform point)
    {
        if (point == rightPullPoint)
        {
            return new Vector3(-1, 0, 0).normalized;
        }
        else
        {
            return new Vector3(1, 0, 0).normalized;
        }
    }

    public override Vector3 GetDirectionToActionPoint(Transform player, Transform pullPoint)
    {
        if (Vector3.Distance(player.position, pullPoint.position) >= Vector3.Distance(pullPoint.position, navPointsDict[pullPoint].position))
        {
            return (navPointsDict[pullPoint].position - player.position).normalized;
        }
        else
        {
            return (pullPoint.position - player.position).normalized;
        }
    }

}
