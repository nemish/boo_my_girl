using UnityEngine;
using System.Collections;

public class Locomotion : MonoBehaviour {
	
	public float speed = 15f;
	public float rotationSpeed = 6f;
	public float maxMovementSpeed = 4f;
	public float maxAttackMoveSpeed = 2f;
	public float minStrength = 1f;
	public float rotationAccuracyAngle = 2f;
	public float runSpeedRatio = 1.5f;
	public float sneakSpeedRatio = 0.5f;
	
	public enum states {Idle, Patrol, AttackPreparing, Attacking, Dragging, DragPreparing}
	public states currentState = states.Idle;
	//Коэф. распространения шума
	public float noiseCoefficient;
	private float noiseCoefficientDefault;
	//Таг определябщий кто слышит издаваемый шум
	public string listenerTag;
	
	public Transform target;
	public GameObject waveEffect;

	public float scaleBegin;
	private float scaleBeginDefalut;

	public float scaleEnd;
	private float scaleEndDefault;

	private float timeScale;
	
	protected DirectInput inputHandler;
	protected Vector3 direction;
	protected Vector3 bodyDirection;
	protected Vector3 oPosition;
	protected float currentSpeedLimit;
	protected float moveSpeedRatio = 1f;
	protected GameObject noise;
	
	void Start () {
		noiseCoefficientDefault = noiseCoefficient;
		scaleBeginDefalut = scaleBegin;
		scaleEndDefault = scaleEnd;
		timeScale = 0.5f;
		Initialize();
	}
	
	public virtual void Initialize() {
		oPosition = transform.position;
		inputHandler = Camera.main.GetComponent<DirectInput>();
	}
	
	void FixedUpdate () {
		ProcessInput();
		ProcessState();
	}
	
	public void SetRunMovement ()
	{
		timeScale = 0.05f;
		noiseCoefficient = noiseCoefficientDefault*2.0f;
		scaleBegin = scaleBeginDefalut * 1.5f;
		scaleEnd = scaleEndDefault*1.5f;
		if (!IsDragging() && !IsDragPreparing())
		{
			moveSpeedRatio = runSpeedRatio;
		}
	}
	
	public void SetSneakMovement ()
	{
		noiseCoefficient = 0.0f;
		scaleBegin = 0.0f;
		scaleEnd = 0.0f;

		if (!IsDragging() && !IsDragPreparing())
		{
			moveSpeedRatio = sneakSpeedRatio;
		}
	}
	
	public void SetCommonMovement ()
	{
		moveSpeedRatio = 1f;
		timeScale = 0.5f;
		noiseCoefficient = noiseCoefficientDefault;
		scaleBegin = scaleBeginDefalut;
		scaleEnd = scaleEndDefault;
	}
	
	protected virtual void ProcessState()
	{
		if (IsAttacking())
		{
			Attack();
		}
		else if (IsAttackPreparing())
		{
			AttackPreparing();
		}
		else
		{
			Idle();
		}
	}
	
	void ProcessInput ()
	{
		UpdateMoveDirection();
		UpdateBodyDirection();
		HandleActions();
		HandleMouse();
	}
	
	protected virtual void UpdateMoveDirection () {}
	
	protected virtual void UpdateBodyDirection () {}
	
	protected virtual void HandleActions() {}
	protected virtual void HandleMouse() {}
	
	public bool IsAttacking()
	{
		return CheckState(states.Attacking);
	}
	
	public bool IsAttackPreparing()
	{
		return CheckState(states.AttackPreparing);
	}
	
	public bool IsIdle()
	{
		return CheckState(states.Idle);
	}
	
	public virtual void ToIdle()
	{
		SetState(states.Idle);
	}
	
	public void ToAttackPreparing()
	{
		SetState(states.AttackPreparing);
	}
	
	public void ToAttacking()
	{
		SetState(states.Attacking);
	}
	
	public void SetState(states state)
	{
		currentState = state;
	}
	
	public bool CheckState(states state)
	{
		return currentState == state;
	}
	
	protected virtual void AttackMovement() {}
	
	protected virtual void CommonMovement()
	{
		if (direction.magnitude > 0)
		{
			Move(direction);
			Rotate(GetRotationDirection());
		}
	}
	
	protected virtual Vector3 GetRotationDirection()
	{
		return direction;
	}
	
	public virtual void ReactToNoise(Transform source) {
		
	}
	
	protected void MakeNoise(Vector3 center, float radius) {
		Collider[] hitColliders = Physics.OverlapSphere(center, radius);
		int i = 0;
		while (i < hitColliders.Length) {
			Collider obj = hitColliders[i];
			
			if(obj.tag == listenerTag) {
				obj.gameObject.GetComponent<Locomotion>().ReactToNoise(transform);
				//Debug.DrawLine(transform.position, obj.transform.position, Color.red, 2.0f);
			}
			i++;
		}
		if (!noise) {
			Vector3 noise_pos = transform.position;
			noise_pos.y = 0.2f;
			noise = Instantiate (waveEffect, noise_pos , Quaternion.identity) as GameObject;
			Vector3 v = noise.transform.position;
			Vector3 scale = noise.transform.localScale;
			scale.x = scaleBegin;
			scale.z = scaleBegin;
			noise.transform.localScale = scale;
			noise.transform.Rotate(Vector3.right, -90.0f);
			noise.transform.position = v;
			StartCoroutine (WaveAnimation (scaleEnd));
		}
	}
	
	IEnumerator WaveAnimation(float wave_radius)
	{
		Vector3 scale = noise.transform.localScale;
		Vector3 scale_end = noise.transform.localScale;
		scale_end.x = wave_radius;
		scale_end.z = wave_radius;
		while(scale.x < (wave_radius-wave_radius/10) && scale.z < (wave_radius-wave_radius/10)) {
			scale = Vector3.Lerp(scale, scale_end, Time.deltaTime*3.0f);
			noise.transform.localScale = scale;
			yield return null;
		}
		noise.renderer.enabled = false;
		yield return new WaitForSeconds(timeScale);
		Destroy (noise);
	}
	
	protected void Move(Vector3 dir)
	{
		if(IsOnMaxSpeed())
		{
			rigidbody.velocity = rigidbody.velocity.normalized * currentSpeedLimit * moveSpeedRatio;
		} else {
			rigidbody.AddForce(dir * speed * moveSpeedRatio);
		}
		MakeNoise (transform.position, noiseCoefficient);
	}
	
	protected void Rotate (Vector3 direction, float rotationRatio = 1f) {
		rigidbody.MoveRotation(Quaternion.Lerp(
			rigidbody.rotation, Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z)), rotationSpeed * rotationRatio * Time.deltaTime
			));
	}
	
	public void Stop()
	{
		direction = Vector3.zero;
		rigidbody.velocity = direction;
	}
	
	
	public bool IsOnMaxSpeed()
	{
		return rigidbody.velocity.magnitude >= currentSpeedLimit * moveSpeedRatio;
	}
	
	protected virtual void AttackPreparing () {}
	
	protected virtual void Attack() {}
	
	protected virtual void Idle () {
		currentSpeedLimit = maxMovementSpeed;
		CommonMovement();
	}
	
	protected virtual bool IsDragging()
	{
		return CheckState(states.Dragging);
	}
	
	protected virtual bool IsDragPreparing()
	{
		return CheckState(states.DragPreparing);
	}
	
	protected virtual void ToDragging()
	{
		SetState(states.Dragging);
	}
	
	protected virtual void ToDragPreparing()
	{
		SetState(states.DragPreparing);
	}
	
}