﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GirlMovement : Locomotion2D
{
    public float maxActionDistance = 2.5f;
    public float maxDraggingSpeed = 1f;
    public float oPositionAccuracyRatio = 0.2f;
    public GameObject mainCanvas;

    protected BoxCollider bodyCol;
    protected SphereCollider footCol;
    protected GameObject bodyRenderer;
    protected Enemy enemy;
    private Animator anim;

    private Transform actableObj;
    private float actableObjOffset;
    private Shelter availableShelterAPI;
    private ActableObject actableObjAPI;
    private Transform dragPoint;
    private List<Transform> owningObjects = new List<Transform>();
    private Transform gun;
    private MainCanvas mainCanvasAPI;
    private Transform shootTarget;
    private bool IsShootInProgress = false;

    public override void Initialize()
    {
        base.Initialize();
        speed = 25f;
        bodyCol = transform.GetComponent<BoxCollider>();
        footCol = transform.GetComponent<SphereCollider>();
        bodyRenderer = transform.Find("Char1_mesh").gameObject;
        anim = bodyRenderer.GetComponent<Animator>();
        mainCanvasAPI = mainCanvas.GetComponent<MainCanvas>();
    }

    protected override void ProcessState()
    {
        if (IsDragging())
        {
            DragObject();
        }
        else if (IsDragPreparing())
        {
            PrepareDragging();
        }
        else
        {
            Idle();
        }
    }

    protected override void UpdateMoveDirection()
    {
        if (IsInShelter())
        {
            Stop();
            return;
        }
        if (IsDragPreparing())
        {
            direction = actableObjAPI.GetDirectionToActionPoint(transform, dragPoint);
            return;
        }
        direction = inputHandler.GetMoveDirection();
        if (!IsDragging() && Mathf.Abs(oPosition.z - transform.position.z) > oPositionAccuracyRatio)
        {
            direction = new Vector3(transform.position.x, transform.position.y, oPosition.z) - transform.position;
        }
        else if (IsDragging() && IsPullingObject())
        {
            if (Mathf.Abs(actableObj.position.x - transform.position.x) > actableObjOffset * 1.2f)
            {
                ToIdle();
                return;
            }
        }
        base.UpdateMoveDirection();
    }

    protected override void HandleActions()
    {
        inputHandler.ProcessActionsInput();
    }

    protected override void HandleMouse()
    {
        inputHandler.ProcessMouseInput();
    }

    public void SetAvailableShelter(Shelter sh)
    {
        availableShelterAPI = sh;
    }

    public void SetActableObject(Transform obj) {
        actableObj = obj;
        if (actableObj)
        {
            if (Vector3.Distance(actableObj.position, transform.position) < maxActionDistance)
            {
                if (!actableObjAPI)
                {
                    actableObjAPI = actableObj.GetComponent<ActableObject>();
                }
                if (actableObj.tag == "Gun")
                {
                    PickupObject(actableObj);
                    actableObj = null;
                }
                else
                {
                    if (!dragPoint)
                    {
                        dragPoint = actableObjAPI.GetActionPoint(transform);
                    }
                    if (!IsDragging() && actableObjAPI)
                    {
                        ToDragPreparing();
                    }
                }
            }
        }
        else
        {
            ToIdle();
        }
    }

    public void SetShootObject(Transform en)
    {
        shootTarget = en;
        if (!IsShootInProgress)
        {
            StartCoroutine("ProcessShoot");
        }
    }

    private void PickupObject(Transform obj)
    {
        gun = obj;
        gun.position = new Vector3(999, 999, 999);
        gun.rigidbody.useGravity = false;
        owningObjects.Add(obj);
        mainCanvasAPI.SetHasGun();
    }

    private void PrepareDragging()
    {
        if (actableObjAPI)
        {
            CommonMovement();
            if (Vector3.Distance(transform.position, dragPoint.position) < 0.3f)
            {
                Stop();
                actableObjOffset = CalcActableObjOffset();
                ToDragging();
            }
        }
        else
        {
            ToIdle();
        }
    }

    public void HandleShelter()
    {
        if (availableShelterAPI)
        {
            if (IsInShelter())
            {
                GetOutFromShelter();
            }
            else
            {
                Stop();
                GetIntoShelter();
            }
        }
    }

    private void GetIntoShelter()
    {
        rigidbody.useGravity = false;
        bodyCol.enabled = false;
        footCol.enabled = false;
        bodyRenderer.SetActive(false);
        availableShelterAPI.SetOccupied();
        transform.position = new Vector3(availableShelterAPI.transform.position.x, transform.position.y, availableShelterAPI.transform.position.z);
    }

    private void GetOutFromShelter()
    {
        transform.position = new Vector3(availableShelterAPI.transform.position.x, oPosition.y, oPosition.z);
        rigidbody.useGravity = true;
        bodyCol.enabled = true;
        footCol.enabled = true;
        bodyRenderer.SetActive(true);
        availableShelterAPI.SetVacant();
    }

    public bool IsInShelter()
    {
        return !bodyCol.enabled;
    }

    private void DragObject()
    {
        currentSpeedLimit = maxDraggingSpeed;
        CommonMovement();
        actableObj.rigidbody.velocity = rigidbody.velocity * 1.1f;
    }

    private float CalcActableObjOffset()
    {
        return Mathf.Abs(transform.position.x - actableObj.position.x);
    }

    public override void ToIdle()
    {
        ResetActableObject();
        anim.SetBool("Sneak", moveSpeedRatio == sneakSpeedRatio);
        anim.SetFloat("Speed", 0);
        base.ToIdle();
    }

    private void ResetActableObject()
    {
        actableObj = null;
        actableObjAPI = null;
        dragPoint = null;
    }

    protected override Vector3 GetRotationDirection()
    {
        if (IsDragging())
        {
            return actableObjAPI.GetFacingDirectionForAction(dragPoint);
        }
        return direction;
    }

    protected override void CommonMovement()
    {
        base.CommonMovement();
        anim.SetBool("Sneak", moveSpeedRatio == sneakSpeedRatio);
        anim.SetFloat("Speed", direction.magnitude);
    }

    private bool IsPullingObject()
    {
        if (facingRight)
        {
            return direction.x < 0;
        }
        else
        {
            return direction.x > 0;
        }
    }

    protected override void UpdateFacing()
    {
        facingRight = transform.rotation.eulerAngles.y < 180f;
    }

    IEnumerator ProcessShoot()
    {
        IsShootInProgress = true;
        while (shootTarget)
        {
            yield return null;
        }
        IsShootInProgress = false;
    }
}
