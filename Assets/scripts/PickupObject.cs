﻿using UnityEngine;
using System.Collections;

public class PickupObject : ActableObject {
    public GameObject hint;

    private TextMesh _hintText;

    void Start()
    {
        _hintText = hint.GetComponent<TextMesh>();
        hint.SetActive(false);
    }

    public void ShowHint()
    {
        hint.SetActive(true);
    }

    public void HideHint()
    {
        hint.SetActive(false);
    }
}
